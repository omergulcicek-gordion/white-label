## Kullanımı

Token'ı ekler yada siler.

```js
setAuthorizationToken(token)    //ekler
setAuthorizationToken(false)    //siler
```
