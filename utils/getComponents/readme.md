Backend'den gelen data sıralı olmayabilir. Örneğin, layoutlarda 1. alana gelecek component, BE'den gelen data'da 3. sırada gelebilir.
Bu fonksiyon parametre aldığı id ve data ile render edilecek componentini bulur ve return eder.

Eğer ilgili component bulunamadıysa bir üst klasöre giderek default component'i return eder.

Default componentte yoksa hata mesajı basar.

# Kullanımı

Bu kod örneği, **data** dizisinde **1** id'sine sahip componenti bulur render eder;

```js
import GetComponents from "@/utils/getComponents"

<GetComponents id={1} data={data} />
```