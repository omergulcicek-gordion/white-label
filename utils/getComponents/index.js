import dynamic from "next/dynamic"

export default function ({ id, data }) {
  const getTypeVersion = (id) => {
    const index = data.findIndex((e) => e.id == id)

    return { type: data[index].type, version: data[index].version }
  }

  let { type, version } = getTypeVersion(id)

  const Comp = dynamic(() =>
    import(`./../../components/${type}/v${version}/index.js`).catch(() => {
      return import(`./../../components/${type}/index.js`).catch(() => {
        return () => <p>{`${type} bileşeni bulunamadı`}</p>
      })
    })
  )

  return <Comp />
}
