export default function (req, res) {
  // TODO NextJS pages/api'de cacheleme yapıyor. Network status 304 dönüyor.

  res.status(200).json({
    content: "Ömer Gülçiçek",
    attributes: {
      mounth: "Şubat",
      year: 2022,
    },
    styles: {
      backgroundColor: "blue",
      color: "blue",
      borderRadius: "20px",
      hover: {
        backgroundColor: "yellow",
      },
      hoverBackgroundColor: "yellow",
    },
  })
}
