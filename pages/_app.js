import Head from "next/head"
import "normalize.css"
import "react-loading-skeleton/dist/skeleton.css"
import "./../public/css/bootstrap-grid.min.css"
import { createGlobalStyle } from "styled-components"

const GlobalStyle = createGlobalStyle`
  // https://github.com/vercel/next.js/blob/canary/examples/with-styled-components/pages/_app.js

  body {
    font-family: sans-serif
  }

  figure {
    margin: 0;
  }

  svg {
    vertical-align: middle;
  }

  :root {
    --c-primary: 26, 62, 120;
  }
`

export default function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Gordion - White Label</title>
        <meta charSet="utf-8" />
        <meta name="description" content="Gordion - White Label Description" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
      </Head>

      <GlobalStyle />
      <Component {...pageProps} />
    </>
  )
}
