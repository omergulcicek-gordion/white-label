import dynamic from "next/dynamic"

export default function Index({ token, layout, version, components }) {
  const Comp = dynamic(() => import(`../layouts/${layout}/v${version}`))

  return <Comp data={components} token={token} />
}

export async function getStaticProps() {
  // TODO istek atılacak, kullanıcının hangi layout'u kullandığı vs getirilecek

  const data = {
    token:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
    status: true,
    layout: "otelSearchForm",
    version: 1,
    components: [
      {
        id: 3,
        type: "button",
        version: 2,
      },
      {
        id: 1,
        type: "numberOfRoomsPeople",
        version: 1,
      },
      {
        id: 2,
        type: "navbar",
        version: 0,
      },
    ],
  }

  return { props: data }
}

//#region
// TODO Parent sitenin özellikleri çekilecek
// export const obj = {
//   url: "https://whitelabel.gordion.tech"
// }
//#endregion
