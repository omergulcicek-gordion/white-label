import GetComponents from "@/utils/getComponents"
import { setAuthorizationToken } from "@/utils/setAuthorizationToken"

export default function App({ data, token }) {
  setAuthorizationToken(token)

  return (
    <>
      <div className="container">
        <GetComponents id={1} data={data} />
      </div>
    </>
  )
}
