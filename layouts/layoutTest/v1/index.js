import GetComponents from "@/utils/getComponents"
import { setAuthorizationToken } from "@/utils/setAuthorizationToken"

export default function App({ data, token }) {
  setAuthorizationToken(token)
  
  return (
    <>
      <div className="container">
        <h1>LayoutTest - v1</h1>

        <div className="row">
          <div
            className="col-12"
            style={{
              padding: "18px",
              border: "1px solid",
              marginBottom: "8px",
            }}
          >
            <h2>1</h2>

            <GetComponents id={1} data={data} />
          </div>
          <div
            className="col-12"
            style={{
              padding: "18px",
              border: "1px solid",
              marginBottom: "8px",
            }}
          >
            <h2>2</h2>

            <GetComponents id={2} data={data} />
          </div>
          <div
            className="col-12"
            style={{
              padding: "18px",
              border: "1px solid",
              marginBottom: "8px",
            }}
          >
            <h2>3</h2>

            <GetComponents id={3} data={data} />
          </div>
        </div>
      </div>
    </>
  )
}
