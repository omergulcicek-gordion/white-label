export default function App(props) {
  return (
    <>
      <div className="container">
        <h1>LayoutTest - v2</h1>

        <div className="row">
          <div
            className="col-6"
            style={{ padding: "18px", border: "1px solid" }}
          >
            1
          </div>
          <div
            className="col-3"
            style={{ padding: "18px", border: "1px solid" }}
          >
            2
          </div>
          <div
            className="col-3"
            style={{ padding: "18px", border: "1px solid" }}
          >
            3
          </div>
        </div>
      </div>
    </>
  )
}
