import axios from "axios"
import useSWR from "swr"
import Skeleton from "react-loading-skeleton"
import styled, { ThemeProvider } from "styled-components"

export default function App({ className, children, ...attr }) {
  const { data, error } = useSWR(
    ["https://jsonplaceholder.typicode.com/users", { token: "abc", id: 3 }],
    fetcher
  )

  if (error) return <small>Failed to load</small>
  if (!data) return <Skeleton height={60} width={240} />

  const css = Object.assign(defaultStyles, dummyStyles)

  return (
    <ThemeProvider theme={css}>
      <Button className={className} {...attr}>
        {children}
      </Button>
    </ThemeProvider>
  )
}

const Button = styled.button`
  align-items: center;
  background-color: ${(props) => props.theme.backgroundColor};
  border: 1px solid rgba(0 0 0 / 10%);
  border-radius: ${(props) => props.theme.borderRadius};
  color: ${(props) => props.theme.color};
  cursor: pointer;
  display: inline-flex;
  font-size: 14px;
  font-weight: 600;
  height: 48px;
  justify-content: center;
  letter-spacing: 0.005em;
  line-height: 48px;
  outline: none;
  padding-left: 16px;
  padding-right: 16px;
  position: relative;
  text-align: center;
  text-decoration: none;
  transition: 0.2s ease-out;
  user-select: none;

  svg {
    pointer-events: none
  }

  &.mini {
    height: 24px;
    line-height: 24px;
    padding: 0;
    width: 24px;
  }

  &.small {
    font-size: 10px;
    height: 32px;
    line-height: 32px;
    letter-spacing: 0.04em;
    padding-left: 24px;
    padding-right: 24px;
  }
  
  &[disabled] {
    background-color #CCD0D8;
    pointer-events: none;
    user-select:none
  }

  &:hover {
    background-color: ${(props) => props.theme.hover.backgroundColor};
  }

  &:active {
    background-color: ${(props) => props.theme.active.backgroundColor};
  }
`

const defaultStyles = {
  color: "#fff",
  backgroundColor: "rgb(var(--c-primary))",
  borderRadius: "4px",
  hover: {
    backgroundColor: "",
  },
  active: {
    backgroundColor: "",
  },
}

const fetcher = async (url, params) => {
  const result = await axios
    .get(url, params)
    .then((res) => res.data)
    .catch((err) => console.error(err))

  //await delay(2500)

  return result
}

//#region
function delay(delayInms) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(2)
    }, delayInms)
  })
}

export const dummyStyles = {}
//#endregion
