import axios from "axios"
import useSWR from "swr"
import Skeleton from "react-loading-skeleton"
import styled, { ThemeProvider } from "styled-components"

export default function App() {
  const { data, error } = useSWR(
    ["https://jsonplaceholder.typicode.com/users", { token: "abc", id: 1 }],
    fetcher
  )

  if (error) return <small>Failed to load</small>
  if (!data) return <Skeleton height={40} width={500} />

  const css = Object.assign(defaultStyles, {})

  const listItems = dummyData?.map(({ href, text }) => (
    <li key={text}>
      <a href={href}>{text}</a>
    </li>
  ))

  return (
    <ThemeProvider theme={css}>
      <Navbar>{listItems}</Navbar>
    </ThemeProvider>
  )
}

const Navbar = styled.ul`
  list-style: none;

  li {
    align-items: center;
    display: inline-flex;
  }

  a {
    color: var(--primary);
    display: inline-flex;
    text-decoration: none;
    padding: 8px;
  }
`

const defaultStyles = {}

const fetcher = async (url, params) => {
  const result = await axios
    .get(url, params)
    .then((res) => res.data)
    .catch((err) => console.error(err))

  await delay(1500)

  return result
}

//#region
function delay(delayInms) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(2)
    }, delayInms)
  })
}

export const dummyData = [
  {
    text: "Ana Sayfa",
    href: "/",
  },
  {
    text: "Blog",
    href: "/blog",
  },
  {
    text: "About",
    href: "/about",
  },
]
//#endregion
