import { useState, useEffect } from "react"
import axios from "axios"
import useSWR from "swr"
import Skeleton from "react-loading-skeleton"
import Button from "@/components/button"
import styled, { ThemeProvider } from "styled-components"

export default function App(props) {
  const maxLimit = { people: 5, child: 3, room: 4 }

  const [isOpen, setOpen] = useState(false)

  const [data, setData] = useState([
    {
      people: 1,
      child: 0,
    },
  ])

  const [totals, setTotals] = useState({ people: 1, child: 0, rooms: 1 })

  useEffect(() => {
    setTotals({
      people: data.reduce((a, b) => ({ people: a.people + b.people })).people,
      child: data.reduce((a, b) => ({ child: a.child + b.child })).child,
      rooms: data.length,
    })
  }, [data])

  const changeData = (e, index) => {
    let setNewValue,
      nameAttr = e.name.split(".")[1]

    if (nameAttr == "people") {
      if (e.value > maxLimit.people) {
        setNewValue = maxLimit.people
      } else if (e.value > 0) {
        setNewValue = e.value * 1
      } else setNewValue = 1
    } else if (nameAttr == "child") {
      setNewValue = e.value > maxLimit.child ? maxLimit.child : e.value * 1
    }

    let newArr = data.map((item, i) => {
      if (index == i) {
        return { ...item, [nameAttr]: setNewValue }
      } else {
        return item
      }
    })

    setData(newArr)
  }

  const setDataByIndex = (e, index, process) => {
    let newArr = data.map((item, i) => {
      if (index == i) {
        if (process == "+") return { ...item, [e.name]: e.value + 1 }
        if (process == "-") return { ...item, [e.name]: e.value - 1 }
        else return item
      } else {
        return item
      }
    })

    setData(newArr)
  }

  //   const { data, error } = useSWR(
  //     ["https://jsonplaceholder.typicode.com/users", { token: "abc", id: 3 }],
  //     fetcher
  //   )

  //   if (error) return <small>Failed to load</small>
  //   if (!data) return <Skeleton height={60} width={240} />

  const css = Object.assign(defaultStyles, dummyStyles)

  return (
    <ThemeProvider theme={css}>
      <Input onClick={() => setOpen(!isOpen)} className={!isOpen && "isOpen"}>
        <SpaceBetween>
          <Info>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M3.49999 8C3.50019 7.05191 3.74546 6.11998 4.21201 5.29463C4.67855 4.46928 5.35053 3.77855 6.16273 3.28947C6.97492 2.80038 7.89976 2.52955 8.84748 2.50326C9.7952 2.47697 10.7336 2.69611 11.5717 3.13942C12.4097 3.58273 13.119 4.23514 13.6306 5.03336C14.1421 5.83157 14.4387 6.74848 14.4915 7.69509C14.5442 8.64171 14.3514 9.58589 13.9317 10.436C13.5119 11.2861 12.8796 12.0133 12.096 12.547C13.7835 13.1659 15.2472 14.2757 16.2986 15.7335C17.3501 17.1913 17.9412 18.9304 17.996 20.727C17.9989 20.8255 17.9825 20.9236 17.9475 21.0157C17.9125 21.1079 17.8598 21.1922 17.7922 21.2639C17.6558 21.4088 17.4674 21.4935 17.2685 21.4995C17.0696 21.5055 16.8764 21.4322 16.7316 21.2957C16.5867 21.1593 16.502 20.9709 16.496 20.772C16.4364 18.8235 15.6205 16.9747 14.2212 15.6174C12.8218 14.2602 10.9489 13.5011 8.99949 13.5011C7.05004 13.5011 5.17718 14.2602 3.77782 15.6174C2.37845 16.9747 1.56254 18.8235 1.50299 20.772C1.49298 20.9678 1.40667 21.152 1.26255 21.2849C1.11843 21.4179 0.927967 21.4891 0.731962 21.4834C0.535957 21.4776 0.350001 21.3953 0.21393 21.2541C0.0778594 21.113 0.00249738 20.9241 0.00398901 20.728C0.0585404 18.9312 0.649596 17.1919 1.70107 15.7339C2.75255 14.2759 4.21633 13.166 5.90399 12.547C5.1627 12.0428 4.55606 11.3648 4.13703 10.5722C3.71799 9.77962 3.49928 8.89653 3.49999 8ZM8.99999 4C7.93912 4 6.92171 4.42143 6.17156 5.17157C5.42142 5.92172 4.99999 6.93913 4.99999 8C4.99999 9.06087 5.42142 10.0783 6.17156 10.8284C6.92171 11.5786 7.93912 12 8.99999 12C10.0609 12 11.0783 11.5786 11.8284 10.8284C12.5786 10.0783 13 9.06087 13 8C13 6.93913 12.5786 5.92172 11.8284 5.17157C11.0783 4.42143 10.0609 4 8.99999 4Z"
                fill="rgb(var(--c-primary))"
              />
              <path
                d="M17.29 8C17.142 8 16.998 8.01 16.856 8.03C16.7569 8.04775 16.6552 8.04535 16.557 8.02295C16.4588 8.00054 16.3661 7.95859 16.2845 7.89959C16.2028 7.84059 16.1339 7.76575 16.0819 7.67954C16.0298 7.59333 15.9956 7.49751 15.9814 7.39781C15.9671 7.2981 15.9731 7.19655 15.999 7.0992C16.0248 7.00186 16.07 6.91072 16.1318 6.83123C16.1937 6.75173 16.2709 6.68551 16.3589 6.6365C16.4469 6.5875 16.5439 6.55672 16.644 6.546C17.6386 6.4022 18.6527 6.5932 19.5269 7.08892C20.401 7.58465 21.0855 8.35697 21.4726 9.28433C21.8597 10.2117 21.9275 11.2414 21.6653 12.2115C21.403 13.1816 20.8256 14.037 20.024 14.643C21.2024 15.1706 22.2029 16.028 22.9049 17.1116C23.6068 18.1953 23.9802 19.4589 23.98 20.75C23.98 20.9489 23.901 21.1397 23.7603 21.2803C23.6197 21.421 23.4289 21.5 23.23 21.5C23.0311 21.5 22.8403 21.421 22.6997 21.2803C22.559 21.1397 22.48 20.9489 22.48 20.75C22.4799 19.6341 22.1203 18.548 21.4546 17.6525C20.7889 16.757 19.8525 16.0997 18.784 15.778L18.25 15.618V13.942L18.66 13.733C19.2678 13.4251 19.754 12.9215 20.0403 12.3032C20.3266 11.685 20.3962 10.9884 20.2378 10.3258C20.0795 9.66314 19.7025 9.07321 19.1676 8.65123C18.6327 8.22924 17.9713 7.99982 17.29 8Z"
                fill="rgb(var(--c-primary))"
              />
            </svg>
            <div>
              <p>Oda ve Kişi Sayısı</p>
              <strong>
                {totals.rooms} oda,
                {totals.people > 0 && ` ${totals.people} yetişkin`}
                {totals.child > 0 && `, ${totals.child} çocuk`}
              </strong>
            </div>
          </Info>

          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="10"
            height="6"
            viewBox="0 0 10 6"
            fill="none"
            className={isOpen && "rotate"}
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M0.480028 5.18667C0.386394 5.09292 0.333801 4.96583 0.333801 4.83333C0.333801 4.70083 0.386394 4.57375 0.480028 4.48L4.64669 0.313333C4.74044 0.219699 4.86753 0.167107 5.00003 0.167107C5.13253 0.167107 5.25961 0.219699 5.35336 0.313333L9.52003 4.48C9.60835 4.57478 9.65643 4.70015 9.65414 4.82968C9.65186 4.95922 9.59938 5.08281 9.50778 5.17441C9.41617 5.26602 9.29258 5.3185 9.16304 5.32078C9.03351 5.32307 8.90814 5.27499 8.81336 5.18667L5.00003 1.37333L1.18669 5.18667C1.09294 5.2803 0.965862 5.33289 0.833361 5.33289C0.700861 5.33289 0.573778 5.2803 0.480028 5.18667Z"
              fill="rgb(var(--c-primary))"
            />
          </svg>
        </SpaceBetween>
      </Input>

      <NumberOfRoomsPeople hidden={isOpen}>
        <form
          onSubmit={(e) => {
            e.preventDefault()
            setOpen(!isOpen)
          }}
        >
          {
            <>
              {data.length > 0 &&
                data.map((room, index) => (
                  <RoomSection key={index}>
                    <div className="row">
                      <div className="col-12">
                        <TitleMargin>
                          <SpaceBetween>
                            <RoomTitle>Oda {index + 1}</RoomTitle>

                            {index > 0 && (
                              <DeleteRoom
                                onClick={() => {
                                  setData(data.filter((e, i) => index != i))
                                }}
                              >
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="12"
                                  height="14"
                                  viewBox="0 0 12 14"
                                  fill="none"
                                >
                                  <path
                                    d="M7.2 2.20001C7.2 1.88175 7.07357 1.57653 6.84853 1.35148C6.62348 1.12644 6.31826 1.00001 6 1.00001C5.68174 1.00001 5.37652 1.12644 5.15147 1.35148C4.92643 1.57653 4.8 1.88175 4.8 2.20001H4C4 1.66958 4.21071 1.16087 4.58579 0.785799C4.96086 0.410726 5.46957 0.200012 6 0.200012C6.53043 0.200012 7.03914 0.410726 7.41421 0.785799C7.78929 1.16087 8 1.66958 8 2.20001L11.6 2.20001C11.7061 2.20001 11.8078 2.24216 11.8828 2.31717C11.9579 2.39218 12 2.49393 12 2.60001C12 2.7061 11.9579 2.80784 11.8828 2.88286C11.8078 2.95787 11.7061 3.00001 11.6 3.00001H11.1568L10.12 11.984C10.0749 12.3739 9.88796 12.7337 9.59487 12.9947C9.30177 13.2558 8.92292 13.4001 8.5304 13.4H3.4696C3.07708 13.4001 2.69823 13.2558 2.40513 12.9947C2.11204 12.7337 1.92514 12.3739 1.88 11.984L0.8424 3.00001H0.4C0.30638 3.00004 0.215714 2.96724 0.143793 2.9073C0.0718717 2.84737 0.0232516 2.7641 0.00639992 2.67201L0 2.60001C0 2.49393 0.0421428 2.39218 0.117157 2.31717C0.192172 2.24216 0.293913 2.20001 0.4 2.20001L7.2 2.20001ZM10.3504 3.00001L1.6488 3.00001L2.6744 11.892C2.69698 12.087 2.79048 12.267 2.93712 12.3975C3.08375 12.5281 3.27327 12.6001 3.4696 12.6H8.5304C8.72659 12.5999 8.91592 12.5278 9.06238 12.3972C9.20885 12.2667 9.30224 12.0869 9.3248 11.892L10.3504 3.00001ZM4.8 5.00001C4.996 5.00001 5.16 5.12401 5.1936 5.28721L5.2 5.35041L5.2 10.2504C5.2 10.4432 5.0208 10.6 4.8 10.6C4.604 10.6 4.44 10.476 4.4064 10.3128L4.4 10.2496L4.4 5.35121C4.4 5.15761 4.5792 5.00081 4.8 5.00081V5.00001ZM7.2 5.00001C7.396 5.00001 7.56 5.12401 7.5936 5.28721L7.6 5.35041L7.6 10.2504C7.6 10.4432 7.4208 10.6 7.2 10.6C7.004 10.6 6.84 10.476 6.8064 10.3128L6.8 10.2496L6.8 5.35121C6.8 5.15761 6.9792 5.00081 7.2 5.00081V5.00001Z"
                                    fill="#DF3C44"
                                  />
                                </svg>
                              </DeleteRoom>
                            )}
                          </SpaceBetween>
                        </TitleMargin>
                      </div>
                      <div className="col-12">
                        <TextRangeInput>
                          <label htmlFor={`${index}.people`}>Yetişkin</label>
                          <div>
                            <Button
                              className="mini"
                              disabled={data[index].people == 1 && true}
                              type="button"
                              onClick={() => {
                                setDataByIndex(
                                  {
                                    name: "people",
                                    value: data[index].people,
                                  },
                                  index,
                                  "-"
                                )
                              }}
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="16"
                                height="16"
                                viewBox="0 0 16 16"
                                fill="#fff"
                              >
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M2.99994 8C2.99994 7.86739 3.05262 7.74022 3.14639 7.64645C3.24015 7.55268 3.36733 7.5 3.49994 7.5H12.4999C12.6325 7.5 12.7597 7.55268 12.8535 7.64645C12.9473 7.74022 12.9999 7.86739 12.9999 8C12.9999 8.13261 12.9473 8.25978 12.8535 8.35355C12.7597 8.44732 12.6325 8.5 12.4999 8.5H3.49994C3.36733 8.5 3.24015 8.44732 3.14639 8.35355C3.05262 8.25978 2.99994 8.13261 2.99994 8Z"
                                  fill="#fff"
                                />
                              </svg>
                            </Button>
                            <input
                              name={`${index}.people`}
                              id={`${index}.people`}
                              type="number"
                              min="1"
                              max={maxLimit.people}
                              value={data[index].people}
                              onChange={({ target }) =>
                                changeData(target, index)
                              }
                            />
                            <Button
                              className="mini"
                              disabled={
                                data[index].people >= maxLimit.people && true
                              }
                              type="button"
                              onClick={() => {
                                setDataByIndex(
                                  {
                                    name: "people",
                                    value: data[index].people,
                                  },
                                  index,
                                  "+"
                                )
                              }}
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="10"
                                height="10"
                                viewBox="0 0 10 10"
                                fill="none"
                              >
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M-6.04013e-05 5.16667C-6.0384e-05 5.03406 0.052618 4.90688 0.146386 4.81311C0.240154 4.71935 0.367331 4.66667 0.49994 4.66667L4.33327 4.66667L4.33327 0.833334C4.33327 0.700725 4.38595 0.573548 4.47972 0.47978C4.57349 0.386012 4.70067 0.333334 4.83327 0.333334C4.96588 0.333334 5.09306 0.386012 5.18683 0.47978C5.2806 0.573548 5.33327 0.700725 5.33327 0.833334L5.33327 4.66667L9.16661 4.66667C9.29922 4.66667 9.42639 4.71935 9.52016 4.81311C9.61393 4.90688 9.66661 5.03406 9.66661 5.16667C9.66661 5.29928 9.61393 5.42645 9.52016 5.52022C9.42639 5.61399 9.29921 5.66667 9.16661 5.66667L5.33327 5.66667L5.33327 9.5C5.33327 9.63261 5.28059 9.75979 5.18683 9.85355C5.09306 9.94732 4.96588 10 4.83327 10C4.70066 10 4.57349 9.94732 4.47972 9.85355C4.38595 9.75979 4.33327 9.63261 4.33327 9.5L4.33327 5.66667L0.49994 5.66667C0.367331 5.66667 0.240154 5.61399 0.146386 5.52022C0.0526179 5.42645 -6.04187e-05 5.29927 -6.04013e-05 5.16667Z"
                                  fill="#fff"
                                />
                              </svg>
                            </Button>
                          </div>
                        </TextRangeInput>
                      </div>

                      <div className="col-12">
                        <SpaceBetween>
                          <label htmlFor={`${index}.child`}>Çocuk</label>
                          <div>
                            <Button
                              className="mini"
                              disabled={data[index].child == 0 && true}
                              type="button"
                              onClick={() => {
                                setDataByIndex(
                                  {
                                    name: "child",
                                    value: data[index].child,
                                  },
                                  index,
                                  "-"
                                )
                              }}
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="16"
                                height="16"
                                viewBox="0 0 16 16"
                                fill="#fff"
                              >
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M2.99994 8C2.99994 7.86739 3.05262 7.74022 3.14639 7.64645C3.24015 7.55268 3.36733 7.5 3.49994 7.5H12.4999C12.6325 7.5 12.7597 7.55268 12.8535 7.64645C12.9473 7.74022 12.9999 7.86739 12.9999 8C12.9999 8.13261 12.9473 8.25978 12.8535 8.35355C12.7597 8.44732 12.6325 8.5 12.4999 8.5H3.49994C3.36733 8.5 3.24015 8.44732 3.14639 8.35355C3.05262 8.25978 2.99994 8.13261 2.99994 8Z"
                                  fill="#fff"
                                />
                              </svg>
                            </Button>
                            <input
                              name={`${index}.child`}
                              id={`${index}.child`}
                              type="number"
                              min="0"
                              max={maxLimit.child}
                              value={data[index].child}
                              onChange={({ target }) =>
                                changeData(target, index)
                              }
                            />
                            <Button
                              className="mini"
                              disabled={
                                data[index].child >= maxLimit.child && true
                              }
                              type="button"
                              onClick={() => {
                                setDataByIndex(
                                  {
                                    name: "child",
                                    value: data[index].child,
                                  },
                                  index,
                                  "+"
                                )
                              }}
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="10"
                                height="10"
                                viewBox="0 0 10 10"
                                fill="none"
                              >
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M-6.04013e-05 5.16667C-6.0384e-05 5.03406 0.052618 4.90688 0.146386 4.81311C0.240154 4.71935 0.367331 4.66667 0.49994 4.66667L4.33327 4.66667L4.33327 0.833334C4.33327 0.700725 4.38595 0.573548 4.47972 0.47978C4.57349 0.386012 4.70067 0.333334 4.83327 0.333334C4.96588 0.333334 5.09306 0.386012 5.18683 0.47978C5.2806 0.573548 5.33327 0.700725 5.33327 0.833334L5.33327 4.66667L9.16661 4.66667C9.29922 4.66667 9.42639 4.71935 9.52016 4.81311C9.61393 4.90688 9.66661 5.03406 9.66661 5.16667C9.66661 5.29928 9.61393 5.42645 9.52016 5.52022C9.42639 5.61399 9.29921 5.66667 9.16661 5.66667L5.33327 5.66667L5.33327 9.5C5.33327 9.63261 5.28059 9.75979 5.18683 9.85355C5.09306 9.94732 4.96588 10 4.83327 10C4.70066 10 4.57349 9.94732 4.47972 9.85355C4.38595 9.75979 4.33327 9.63261 4.33327 9.5L4.33327 5.66667L0.49994 5.66667C0.367331 5.66667 0.240154 5.61399 0.146386 5.52022C0.0526179 5.42645 -6.04187e-05 5.29927 -6.04013e-05 5.16667Z"
                                  fill="#fff"
                                />
                              </svg>
                            </Button>
                          </div>
                        </SpaceBetween>
                      </div>
                    </div>
                  </RoomSection>
                ))}
              {maxLimit.room > data.length && (
                <AddRoom
                  onClick={() => {
                    setData([...data, { people: 1, child: 0 }])
                  }}
                >
                  <SpaceBetween>
                    <strong>Oda Ekle</strong>
                    <span>+</span>
                  </SpaceBetween>
                </AddRoom>
              )}
            </>
          }

          <RoomSummary>
            <div>
              <p>{totals.rooms} oda</p>
              <p>{totals.people > 0 && `${totals.people} yetişkin`}</p>
              <p>{totals.child > 0 && `${totals.child} çocuk`}</p>
            </div>
            <Button className="small" type="submit">
              UYGULA
            </Button>
          </RoomSummary>
        </form>
      </NumberOfRoomsPeople>
    </ThemeProvider>
  )
}

const SpaceBetween = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  width: 100%;
`

const NumberOfRoomsPeople = styled.div`
  background: #ffffff;
  border: 1px solid rgb(var(--c-primary));
  border-radius: 4px;
  box-shadow: 0px 4px 16px rgba(60, 138, 255, 0.16);
  padding: 16px;
  max-width: 300px;

  input {
    width: 50px;
  }

  label {
    font-size: 14px;
    line-height: 20px;
  }

  br {
    padding: 0;
    height: 0;
    margin: 0;
    display: inline;
  }
`

const TextRangeInput = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 16px;
`

const Input = styled.div`
  background: #fff;
  border: 1px solid #ccd0d8;
  border-radius: 4px;
  cursor: pointer;
  display: flex;
  align-items: center;
  margin-bottom: 16px;
  padding: 8px 16px;
  height: 48px;
  margin-top: 150px;
  transition: 0.2s;
  width: 300px;

  &.isOpen,
  &:hover {
    border-color: rgb(var(--c-primary));
    box-shadow: 0px 4px 16px rgba(26, 62, 120, 0.16);
  }

  .rotate {
    transform: rotate(180deg);
    transform-origin: center;
  }
`

const Info = styled.div`
  align-items: center;
  display: flex;

  div {
    margin-left: 8px;
  }

  p {
    font-size: 10px;
    line-height: 1;
    letter-spacing: 0.025em;
    color: #99a1b1;
    margin: 0;
  }

  strong {
    font-weight: normal;
    font-size: 14px;
    line-height: 14px;
    letter-spacing: 0.015em;
    color: rgb(var(--c-primary));
  }
`

const RoomSection = styled.section`
  background-color: rgba(var(--c-primary), 0.03);
  border-radius: 4px;
  margin-bottom: 16px;
  padding: 16px;
`

const RoomTitle = styled.strong`
  color: rgb(var(--c-primary));
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.005em;
`
const TitleMargin = styled.div`
  margin-bottom: 8px;
`

const DeleteRoom = styled.figure`
  cursor: pointer;
  padding: 5px 2px;
`

const AddRoom = styled.section`
  background-color: rgba(var(--c-primary), 0.03);
  border-radius: 4px;
  cursor: pointer;
  margin-bottom: 16px;
  padding: 16px;
  transition: 0.1s;

  &:hover {
    background-color: rgba(var(--c-primary), 0.1);
  }

  strong {
    color: rgb(var(--c-primary));
    font-weight: 600;
    font-size: 16px;
    line-height: 24px;
    letter-spacing: 0.005em;
  }
`

const RoomSummary = styled.div`
  align-items: flex-start;
  color: #99a1b1;
  display: flex;
  letter-spacing: 0.025em;
  font-size: 10px;
  justify-content: space-between;
  line-height: 14px;
  font-style: normal;
  font-weight: normal;

  p {
    margin: 0;
  }
`

const defaultStyles = {}

const fetcher = async (url, params) => {
  const result = await axios
    .get(url, params)
    .then((res) => res.data)
    .catch((err) => console.error(err))

  //await delay(2500)

  return result
}

//#region
function delay(delayInms) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(2)
    }, delayInms)
  })
}

export const dummyStyles = {}
//#endregion
